﻿/*
 * Program: Methods Demonstration
 * 
 * Created by: Miles McDonald
 * 
 * Date: 28 Sep 2023
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodsDemoWk5
{
    internal class Program
    {
        static void Main(string[] args)
        {

            double age = ReadValue("Enter your age: ", 0, 70);

            Console.WriteLine("Age: " + age);


            //A method is a code block that contains
            //    a series of statements

            //Method : performs a section of code, whenever
            //           "invoked"

            // No need to retype the code block; just
            //      invoke the method

            Console.ReadLine();

        }

        // We can create our own method OUTSIDE the Main method
        // And then we can call it from within the Main method

        static double ReadValue
            (
            string prompt,  // prompt for the user
            double low,     // lowest allowed value
            double high   // highest allowed value
            )
        {
            double result = 0;
        
               do
            {
                Console.WriteLine(prompt + " between " + low + " and " + high);
                string resultString = Console.ReadLine();
                result = double.Parse(resultString);
            }

            while ((result < low) || (result > high));
            return result;
        }


    }
}
