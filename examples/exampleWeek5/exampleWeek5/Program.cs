﻿/*
 * Project: Week 5
 * 
 * Purpose: Learn Methods and overloading Methods
 * 
 * Revision History:  created by Miles McDonald Sept 22
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exampleWeek5
{
    class Program
    {
        // Heading of Conestoga College
        static void Heading()
        {


            Console.WriteLine("Conestoga College");


        }
        static int Adding(int x, int y)
        {
            // Hint:  DO NOT USE x or y as variable names.  Why?

            return (x + y);
        }

        // Overloaded Method

        static int Adding(int x, int y, int z)
        {
            Console.Write("Please enter a Third Number: ");
            z = int.Parse(Console.ReadLine());

            return (x + y + z);

        }

        static int Adding(int x, int y, int z, int fourthNumber)
        {
            Console.Write("Please enter a Third Number: ");
            z = int.Parse(Console.ReadLine());

            return (x + y + z + fourthNumber);

        }
        // Remember Pascal for Method name

        static int SampleMethod(int a, int b = 3, int c = 4)
        {
            return ((a + b) / c);

        }

   

        static void Main(string[] args)
        {
            // Declare variables

            int returnedValue;


            // initiate variables

            returnedValue = 0;

            Heading();

            returnedValue = Adding(10, 20);
            Console.Write("The answer is: " + returnedValue);

            returnedValue = Adding(10, 20, 0);

            Console.WriteLine("\nThe answer is: " + returnedValue);

            returnedValue = Adding(10, 20, 0, 15);

            Console.WriteLine("\nThe answer is: " + returnedValue);

            returnedValue = SampleMethod(b: 4, c: 5, a: 1);

            Console.WriteLine("\nThe answer is: " + returnedValue);

            returnedValue = SampleMethod(4, 5, 1);

            //What should be the answer?  Why is that?

            Console.WriteLine("\nThe answer is: " + returnedValue);

            returnedValue = SampleMethod(4, 5);

            //What should be the answer?

            Console.WriteLine("\nThe answer is: " + returnedValue);

            Console.ReadKey();
        }
    }
}
