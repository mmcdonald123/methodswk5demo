﻿/*
 * Project: Week 5 Lecture
 * 
 * Purpose: Learn Methods
 * 
 * Revision History: created by Miles McDonald, 4 Oct 22
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exampleWeek5D
{
    internal class Program
    {
        static void Heading()
        {
            Console.WriteLine("Conestoga College");
        }
        static int Adding (int x, int y) //remember static; Adding is the name (meaningful identifier). Body is in curly braces.
        {
            // void means no information and therefore not returning anything

            return (x + y);
        }
        //Overloaded Method
        static int Adding(int x, int y, int z)

        {
            Console.Write("Please enter a Third Number: ");
            z = int.Parse(Console.ReadLine());

            return (x + y + z);
        }

        static int Adding(int x, int y, int z, int fourthNumber)


        {
            Console.Write("Please enter a Third Number: ");
            z = int.Parse(Console.ReadLine());

            return (x + y + z + fourthNumber);
        }



        static void Main(string[] args)
        {

            // Declare Variables

            int returnedValue;

            // initiate variables

            returnedValue = 0;


            Heading();

            returnedValue = Adding(0, 20);

            Console.WriteLine("The answer is: " + returnedValue);

            returnedValue = Adding(10, 20, 0);

            Console.WriteLine("\nThe answer is: " + returnedValue);

            returnedValue = Adding(10, 20, 0, 10);

            Console.WriteLine("\nThe answer is: " + returnedValue);

            Console.ReadKey();

        }
    }
}
